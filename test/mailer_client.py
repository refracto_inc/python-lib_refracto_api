# -*- coding: utf-8 -*-
from __future__ import print_function
import grpc

import mailer_pb2_grpc
import mailer_pb2



class Mailer(object):
    def __init__(self, url="", port=0):
        server_address = f"{url}:{port}"
        self.channel = grpc.insecure_channel(server_address)

    def SendToOne(self, refracto_user_id=0, message=""):
        stub = mailer_pb2_grpc.SendToOneStub(self.channel)
        response = stub.SendOne(
            mailer_pb2.SendRequest(refracto_user_id=refracto_user_id, message=message))
        return response.status

    def SendToAll(self, message=""):
        stub = mailer_pb2_grpc.SendToAllStub(self.channel)
        response = stub.SendAll(
            mailer_pb2.SendRequest(message=message))
        return response.status
