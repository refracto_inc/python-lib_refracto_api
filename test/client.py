# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import sys

import grpc

import refracto_api.engine.user.user_pb2_grpc as user_pb2_grpc
import refracto_api.engine.user.user_pb2 as user_pb2

class User(object):

    #  Get User Info
    def UserAll(self, refracto_user_id):
        stub = user_pb2_grpc.UserStub(self.channel)
        print(self.channel)
        response = stub.UserAll(
            user_pb2.IDs(refracto_user_id=refracto_user_id)
        )
        return response

    def UserProfile(self, refracto_user_id):
        stub = user_pb2_grpc.UserStub(self.channel)
        print(self.channel)
        response = stub.UserProfile(
            user_pb2.IDs(refracto_user_id=refracto_user_id)
        )
        return response

    def UserFinancial(self, refracto_user_id):
        stub = user_pb2_grpc.UserStub(self.channel)
        print(self.channel)
        response = stub.UserFinancial(
            user_pb2.IDs(refracto_user_id=refracto_user_id)
        )
        return response

    def UserAdvanced(self, refracto_user_id):
        stub = user_pb2_grpc.UserStub(self.channel)
        print(self.channel)
        response = stub.UserAdvanced(
            user_pb2.IDs(refracto_user_id=refracto_user_id)
        )
        return response

    #  Edit User Info

    def UserEditSurname(self, refracto_user_id, surname):
        stub = user_pb2_grpc.UserStub(self.channel)
        print(self.channel)
        response = stub.UserEditSurname(
            user_pb2.EditSurname(ids=user_pb2.IDs(refracto_user_id=refracto_user_id), surname=surname)
        )
        return response

class EngineServer(User):
    def __init__(self, url="", port=0):
        server_address = f"{url}:{port}"
        self.channel = grpc.insecure_channel(server_address)
        self.channels = server_address



all = EngineServer(url="192.168.1.20", port=8888).UserAll(refracto_user_id=6736956)
print(all)
# print(EngineServer(url="192.168.1.20", port=8888).UserEditSurname(refracto_user_id=6736956, surname="Sokolov").status)
all = EngineServer(url="192.168.1.20", port=8888).UserFinancial(refracto_user_id=6736956).referrals.users
print(all)