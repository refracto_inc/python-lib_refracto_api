# -*- coding: utf-8 -*-
from __future__ import print_function
import grpc

from refracto_grpc import user as user_pb2_grpc, user as user_pb2


class Mailer(object):
    def __init__(self, url="", port=0):
        server_address = f"{url}:{port}"
        self.channel = grpc.insecure_channel(server_address)

    def SendToOne(self, refracto_user_id=0, message=""):
        stub = mailer_pb2_grpc.SendToOneStub(self.channel)
        response = stub.SendOne(
            mailer_pb2.SendRequest(refracto_user_id=refracto_user_id, message=message))
        return response.status

    def SendToAll(self, message=""):
        stub = mailer_pb2_grpc.SendToAllStub(self.channel)
        response = stub.SendAll(
            mailer_pb2.SendRequest(message=message))
        return response.status


class User(object):

    #  Get User Info
    def UserAll(self, refracto_user_id):
        stub = user_pb2_grpc.UserStub(self.channel)
        print(self.channel)
        response = stub.UserAll(
            user_pb2.IDs(refracto_user_id=refracto_user_id)
        )
        return response

    def UserProfile(self, refracto_user_id):
        stub = user_pb2_grpc.UserStub(self.channel)
        print(self.channel)
        response = stub.UserProfile(
            user_pb2.IDs(refracto_user_id=refracto_user_id)
        )
        return response

    def UserFinancial(self, refracto_user_id):
        stub = user_pb2_grpc.UserStub(self.channel)
        print(self.channel)
        response = stub.UserFinancial(
            user_pb2.IDs(refracto_user_id=refracto_user_id)
        )
        return response

    def UserAdvanced(self, refracto_user_id):
        stub = user_pb2_grpc.UserStub(self.channel)
        print(self.channel)
        response = stub.UserAdvanced(
            user_pb2.IDs(refracto_user_id=refracto_user_id)
        )
        return response

    #  Edit User Info

    def UserEditSurname(self, refracto_user_id, surname):
        stub = user_pb2_grpc.UserStub(self.channel)
        print(self.channel)
        response = stub.UserEditSurname(
            user_pb2.EditSurname(ids=user_pb2.IDs(refracto_user_id=refracto_user_id), surname=surname)
        )
        return response
