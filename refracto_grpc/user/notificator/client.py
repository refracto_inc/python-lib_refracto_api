from . import notificator_pb2_grpc, notificator_pb2
import grpc

class Notificator():
    def __init__(self, url="", port=0):
        server_address = f"{url}:{port}"
        self.channel = grpc.insecure_channel(server_address)

    def SendToOne(self, refracto_user_id=0, message=""):
        stub = notificator_pb2_grpc.SendToOneStub(self.channel)
        response = stub.SendOne(
            notificator_pb2.SendRequest(refracto_user_id=refracto_user_id, message=message))
        return response.status

    def SendToAll(self, message=""):
        stub = notificator_pb2_grpc.SendToAllStub(self.channel)
        response = stub.SendAll(
            notificator_pb2.SendRequest(message=message))
        return response.status
