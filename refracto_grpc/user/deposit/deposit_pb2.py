# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: deposit.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.api import annotations_pb2 as google_dot_api_dot_annotations__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='deposit.proto',
  package='deposit',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\rdeposit.proto\x12\x07\x64\x65posit\x1a\x1cgoogle/api/annotations.proto\"G\n\x04\x41uth\x12\r\n\x05token\x18\x01 \x01(\t\x12\x0f\n\x07session\x18\x02 \x01(\t\x12\x10\n\x08password\x18\x03 \x01(\t\x12\r\n\x05login\x18\x04 \x01(\t\"d\n\x03IDs\x12\x12\n\ninvoice_id\x18\x01 \x01(\x05\x12\x18\n\x10refracto_user_id\x18\x02 \x01(\x05\x12\x12\n\ntg_user_id\x18\x03 \x01(\x05\x12\x1b\n\x04\x61uth\x18\x04 \x01(\x0b\x32\r.deposit.Auth\"\x18\n\x06Status\x12\x0e\n\x06status\x18\x01 \x01(\x05\"L\n\rInvoiceCreate\x12\x19\n\x03ids\x18\x01 \x01(\x0b\x32\x0c.deposit.IDs\x12\x0e\n\x06\x61mount\x18\x02 \x01(\x05\x12\x10\n\x08\x63urrency\x18\x03 \x01(\t\"p\n\x0bInvoiceInfo\x12\x19\n\x03ids\x18\x01 \x01(\x0b\x32\x0c.deposit.IDs\x12\x0e\n\x06status\x18\x02 \x01(\x08\x12\x14\n\x0cinvoice_date\x18\x03 \x01(\t\x12\x0e\n\x06\x61mount\x18\x04 \x01(\x05\x12\x10\n\x08\x63urrency\x18\x05 \x01(\t2\x9e\x02\n\x04Qiwi\x12\x65\n\rCreateInvoice\x12\x16.deposit.InvoiceCreate\x1a\x14.deposit.InvoiceInfo\"&\x82\xd3\xe4\x93\x02 \"\x1b/deposit/Qiwi/CreateInvoice:\x01*\x12T\n\x0c\x43\x61ncelUnpaid\x12\x0c.deposit.IDs\x1a\x0f.deposit.Status\"%\x82\xd3\xe4\x93\x02\x1f\"\x1a/deposit/Qiwi/CancelUnpaid:\x01*\x12Y\n\x0c\x43heckPayment\x12\x0c.deposit.IDs\x1a\x14.deposit.InvoiceInfo\"%\x82\xd3\xe4\x93\x02\x1f\"\x1a/deposit/Qiwi/CheckPayment:\x01*b\x06proto3')
  ,
  dependencies=[google_dot_api_dot_annotations__pb2.DESCRIPTOR,])




_AUTH = _descriptor.Descriptor(
  name='Auth',
  full_name='deposit.Auth',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='token', full_name='deposit.Auth.token', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='session', full_name='deposit.Auth.session', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='password', full_name='deposit.Auth.password', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='login', full_name='deposit.Auth.login', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=56,
  serialized_end=127,
)


_IDS = _descriptor.Descriptor(
  name='IDs',
  full_name='deposit.IDs',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='invoice_id', full_name='deposit.IDs.invoice_id', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='refracto_user_id', full_name='deposit.IDs.refracto_user_id', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='tg_user_id', full_name='deposit.IDs.tg_user_id', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='auth', full_name='deposit.IDs.auth', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=129,
  serialized_end=229,
)


_STATUS = _descriptor.Descriptor(
  name='Status',
  full_name='deposit.Status',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='status', full_name='deposit.Status.status', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=231,
  serialized_end=255,
)


_INVOICECREATE = _descriptor.Descriptor(
  name='InvoiceCreate',
  full_name='deposit.InvoiceCreate',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ids', full_name='deposit.InvoiceCreate.ids', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='amount', full_name='deposit.InvoiceCreate.amount', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='currency', full_name='deposit.InvoiceCreate.currency', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=257,
  serialized_end=333,
)


_INVOICEINFO = _descriptor.Descriptor(
  name='InvoiceInfo',
  full_name='deposit.InvoiceInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ids', full_name='deposit.InvoiceInfo.ids', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='status', full_name='deposit.InvoiceInfo.status', index=1,
      number=2, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='invoice_date', full_name='deposit.InvoiceInfo.invoice_date', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='amount', full_name='deposit.InvoiceInfo.amount', index=3,
      number=4, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='currency', full_name='deposit.InvoiceInfo.currency', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=335,
  serialized_end=447,
)

_IDS.fields_by_name['auth'].message_type = _AUTH
_INVOICECREATE.fields_by_name['ids'].message_type = _IDS
_INVOICEINFO.fields_by_name['ids'].message_type = _IDS
DESCRIPTOR.message_types_by_name['Auth'] = _AUTH
DESCRIPTOR.message_types_by_name['IDs'] = _IDS
DESCRIPTOR.message_types_by_name['Status'] = _STATUS
DESCRIPTOR.message_types_by_name['InvoiceCreate'] = _INVOICECREATE
DESCRIPTOR.message_types_by_name['InvoiceInfo'] = _INVOICEINFO
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Auth = _reflection.GeneratedProtocolMessageType('Auth', (_message.Message,), {
  'DESCRIPTOR' : _AUTH,
  '__module__' : 'deposit_pb2'
  # @@protoc_insertion_point(class_scope:deposit.Auth)
  })
_sym_db.RegisterMessage(Auth)

IDs = _reflection.GeneratedProtocolMessageType('IDs', (_message.Message,), {
  'DESCRIPTOR' : _IDS,
  '__module__' : 'deposit_pb2'
  # @@protoc_insertion_point(class_scope:deposit.IDs)
  })
_sym_db.RegisterMessage(IDs)

Status = _reflection.GeneratedProtocolMessageType('Status', (_message.Message,), {
  'DESCRIPTOR' : _STATUS,
  '__module__' : 'deposit_pb2'
  # @@protoc_insertion_point(class_scope:deposit.Status)
  })
_sym_db.RegisterMessage(Status)

InvoiceCreate = _reflection.GeneratedProtocolMessageType('InvoiceCreate', (_message.Message,), {
  'DESCRIPTOR' : _INVOICECREATE,
  '__module__' : 'deposit_pb2'
  # @@protoc_insertion_point(class_scope:deposit.InvoiceCreate)
  })
_sym_db.RegisterMessage(InvoiceCreate)

InvoiceInfo = _reflection.GeneratedProtocolMessageType('InvoiceInfo', (_message.Message,), {
  'DESCRIPTOR' : _INVOICEINFO,
  '__module__' : 'deposit_pb2'
  # @@protoc_insertion_point(class_scope:deposit.InvoiceInfo)
  })
_sym_db.RegisterMessage(InvoiceInfo)



_QIWI = _descriptor.ServiceDescriptor(
  name='Qiwi',
  full_name='deposit.Qiwi',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=450,
  serialized_end=736,
  methods=[
  _descriptor.MethodDescriptor(
    name='CreateInvoice',
    full_name='deposit.Qiwi.CreateInvoice',
    index=0,
    containing_service=None,
    input_type=_INVOICECREATE,
    output_type=_INVOICEINFO,
    serialized_options=_b('\202\323\344\223\002 \"\033/deposit/Qiwi/CreateInvoice:\001*'),
  ),
  _descriptor.MethodDescriptor(
    name='CancelUnpaid',
    full_name='deposit.Qiwi.CancelUnpaid',
    index=1,
    containing_service=None,
    input_type=_IDS,
    output_type=_STATUS,
    serialized_options=_b('\202\323\344\223\002\037\"\032/deposit/Qiwi/CancelUnpaid:\001*'),
  ),
  _descriptor.MethodDescriptor(
    name='CheckPayment',
    full_name='deposit.Qiwi.CheckPayment',
    index=2,
    containing_service=None,
    input_type=_IDS,
    output_type=_INVOICEINFO,
    serialized_options=_b('\202\323\344\223\002\037\"\032/deposit/Qiwi/CheckPayment:\001*'),
  ),
])
_sym_db.RegisterServiceDescriptor(_QIWI)

DESCRIPTOR.services_by_name['Qiwi'] = _QIWI

# @@protoc_insertion_point(module_scope)
