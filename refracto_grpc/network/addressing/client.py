from . import addressing_pb2_grpc, addressing_pb2
import grpc

class Addressing_UserStack:
    def __init__(self, url="", port=""):
        server_address = f"{url}:{port}"
        self.channel = grpc.insecure_channel(server_address)
        self.stub = addressing_pb2_grpc.UserStackStub(self.channel)

    @property
    def Account(self):
        response = self.stub.Account(
            addressing_pb2.Request(
                requested=True
            )
        )
        return response

    @property
    def Deposit(self):
        response = self.stub.Deposit(
            addressing_pb2.Request(
                requested=True
            )
        )
        return response

    @property
    def Auth(self):
        response = self.stub.Auth(
            addressing_pb2.Request(
                requested=True
            )
        )
        return response

    @property
    def Notificator(self):
        response = self.stub.Notificator(
            addressing_pb2.Request(
                requested=True
            )
        )
        return response

class Addressing_PlatformStack:
    def __init__(self, url="", port=""):
        server_address = f"{url}:{port}"
        self.channel = grpc.insecure_channel(server_address)
        self.stub = addressing_pb2_grpc.PlatformStackStub(self.channel)

    @property
    def Botray(self):
        response = self.stub.Botray(
            addressing_pb2.Request(
                requested=True
            )
        )
        return response

    @property
    def Databases(self):
        response = self.stub.Databases(
            addressing_pb2.Request(
                requested=True
            )
        )
        return response

    @property
    def Payments(self):
        response = self.stub.Payments(
            addressing_pb2.Request(
                requested=True
            )
        )
        return response

class Addressing_DataStorageStack:
    def __init__(self, url="", port=""):
        server_address = f"{url}:{port}"
        self.channel = grpc.insecure_channel(server_address)
        self.stub = addressing_pb2_grpc.DataStorageStackStub(self.channel)

    @property
    def InternalPostgres(self):
        response = self.stub.InternalPostgres(
            addressing_pb2.Request(
                requested=True
            )
        )
        return response

    @property
    def InternalRedis(self):
        response = self.stub.InternalRedis(
            addressing_pb2.Request(
                requested=True
            )
        )
        return response

    @property
    def PostgresService(self):
        response = self.stub.PostgresService(
            addressing_pb2.Request(
                requested=True
            )
        )
        return response

    @property
    def MysqlService(self):
        response = self.stub.MysqlService(
            addressing_pb2.Request(
                requested=True
            )
        )
        return response
